<?php
/**
 * DokuWiki Starter Template
 *
 * @link     http://dokuwiki.org/template:ipari
 * @author   Kwangyoung Lee <ipari@leaflette.com>
 * @license  GPL 2 (http://www.gnu.org/licenses/gpl.html)
 */

if (!defined('DOKU_INC')) die();
@require_once(dirname(__FILE__).'/tpl_functions.php');
header('X-UA-Compatible: IE=edge,chrome=1');
$showSidebar = page_findnearest($conf['sidebar']);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $conf['lang'] ?>"
  lang="<?php echo $conf['lang'] ?>" dir="<?php echo $lang['direction'] ?>" class="no-js">
<head>
    <meta charset="UTF-8" />
    <title><?php tpl_pagetitle() ?> [<?php echo strip_tags($conf['title']) ?>]</title>
    <script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>
    <?php tpl_metaheaders() ?>
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <?php echo tpl_favicon(array('favicon', 'mobile')) ?>
    <?php tpl_includeFile('meta.html') ?>
	<link href="https://fonts.googleapis.com/css2?family=Expletus+Sans:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">

</head>

<?php
$translation = plugin_load('helper','translation');
global $ID;
$startpage = $conf['start'];
$lang2 = $translation->getLangPart($ID);
if ($lang2 != '') {
 $startpage = $lang2 .':'.$startpage;
}
?>
<body id="dokuwiki__top">
    <div id="dokuwiki__site" class="<?php echo tpl_classes(); ?> <?php echo ($showSidebar) ? 'hasSidebar' : ''; ?>">
        <?php tpl_includeFile('header.html') ?>

        <!-- ********** HEADER ********** -->
        <div id="dokuwiki__header">
            <div class="group">

            <?php
                $sitetitle_page = tpl_getConf('sitetitle');
                $title = $lang2 .':'. $sitetitle_page;
                $title_text = tpl_include_page($title, 0, 1);
                $sitetitleshort_page = tpl_getConf('sitetitleshort');
                $title_short = $lang2 .':'. $sitetitleshort_page;
                $title_text_short = tpl_include_page($title_short, 0, 1);


            ?>

                <h1 id="sitetitle"><?php tpl_link('/'.$startpage,$title_text,'accesskey="h" title="[H]"') ?></h1>
                <h1 id="sitetitle-short"><?php tpl_link('/'.$startpage,$title_text_short,'accesskey="h" title="[H]"') ?></h1>



                <div class="left">
                    <?php if ($showSidebar): ?>
                    <button class="btn_left" accesskey="s", title="[S]">Nav</button>
                    <?php endif; ?>
                </div>
                <div class="right">
                    <?php if (!empty($_SERVER['REMOTE_USER'])) { ?><button class="btn_search">Search</button><?php } ?>
                    <button class="btn_right" accesskey="m", title="[M]">Edit</button>
                </div>
            </div>
            <?php if (!empty($_SERVER['REMOTE_USER'])) { ?>
            <div class="search">
                <?php tpl_searchform(); ?>
            </div>
          <?php } ?>
        </div><!-- /header -->

        <!-- ********** sidebar ********** -->
        <div id="sidebar_wrapper">
            <!-- ********** ASIDE ********** -->
            <?php if ($showSidebar): ?>
            <div id="dokuwiki__aside"  class="sidebar">
                <?php tpl_includeFile('sidebarheader.html') ?>
                <?php tpl_include_page($conf['sidebar'], 1, 1) ?>
                <?php tpl_includeFile('sidebarfooter.html') ?>
            </div><!-- /dokuwiki__aside -->
            <?php endif; ?>

            <div id="dokuwiki__tools" class="sidebar left">
              <?php if (!empty($_SERVER['REMOTE_USER'])) { ?>
                <!-- PAGE TOOLS -->
                <div id="dokuwiki__pagetools">
                    <h3><?php echo $lang['page_tools'] ?></h3>
                    <ul>
                        <?php white_toolsevent('pagetools', array(
                            'edit'      => tpl_action('edit', 1, 'li', 1, '<span>', '</span>'),
                            'revisions' => tpl_action('revisions', 1, 'li', 1, '<span>', '</span>'),
                            'backlink'  => tpl_action('backlink', 1, 'li', 1, '<span>', '</span>'),
                            'subscribe' => tpl_action('subscribe', 1, 'li', 1, '<span>', '</span>'),
                            'revert'    => tpl_action('revert', 1, 'li', 1, '<span>', '</span>'),
                        )); ?>
                    </ul>
                </div><!-- /dokuwiki__pagetools -->

                <!-- SITE TOOLS -->

                <div id="dokuwiki__sitetools">
                    <h3><?php echo $lang['site_tools'] ?></h3>
                    <ul>
                        <?php white_toolsevent('sitetools', array(
                            'recent'    => tpl_action('recent', 1, 'li', 1, '<span>', '</span>'),
                            'media'     => tpl_action('media', 1, 'li', 1, '<span>', '</span>'),
                            'index'     => tpl_action('index', 1, 'li', 1, '<span>', '</span>'),
                        )); ?>
                    </ul>
                </div><!-- /dokuwiki__sitetools -->
              <?php } ?>

                <!-- USER TOOLS -->
                <?php if ($conf['useacl']): ?>
                <div id="dokuwiki__usertools">
                    <h3><?php echo $lang['user_tools'] ?></h3>
                    <ul>
                        <?php white_toolsevent('usertools', array(
                            'admin'     => tpl_action('admin', 1, 'li', 1, '<span>', '</span>'),
                            'profile'   => tpl_action('profile', 1, 'li', 1, '<span>', '</span>'),
                            'register'  => tpl_action('register', 1, 'li', 1, '<span>', '</span>'),
                            'login'     => tpl_action('login', 1, 'li', 1, '<span>', '</span>'),
                        )); ?>
                    </ul>
                    <?php
                        if (!empty($_SERVER['REMOTE_USER'])) {
                            echo '<div class="user">';
                            tpl_userinfo();
                            echo '</div>';
                        }
                    ?>
                </div><!-- /dokuwiki__usertools -->
                <?php endif ?>
            </div><!-- /dokuwiki__tools -->

            <div id="sidebar_bg">
            </div>

            <div id="to_top">
                <?php tpl_action('top') ?>
            </div>
        </div><!-- /sidebar_wrapper -->

        <div class="wrapper group">
            <!-- ********** CONTENT ********** -->
            <div id="dokuwiki__content"><div class="group">
                <?php tpl_flush() ?>
                <?php tpl_includeFile('pageheader.html') ?>

                <div class='pagetitle'>
                  <!-- BREADCRUMBS -->
                  <?php if($conf['breadcrumbs']){ ?>
                    <div class="breadcrumbs"><div class="breadcrumbs-content"><?php tpl_breadcrumbs($ret='›') ?></div></div>
                  <?php } ?>
                  <?php if($conf['youarehere']){ ?>
                    <div class="breadcrumbs"><div class="breadcrumbs-content"><?php tpl_youarehere() ?></div></div>
                  <?php } ?>
                  <h1><?php tpl_pagetitle() ?></h1>
                </div>

                <div class="page group
                <?php if(tpl_getConf('numberedHeading')): ?> numbered_heading<?php endif ?>
                <?php if(tpl_getConf('tocPosition')): ?> toc_<?php echo tpl_getConf('tocPosition') ?><?php endif ?>">
                    <!-- wikipage start -->
                    <?php tpl_content() ?>
                    <!-- wikipage stop -->
                </div>

                <?php tpl_flush() ?>
                <?php tpl_includeFile('pagefooter.html') ?>
            </div></div><!-- /content -->

            <!-- ********** FOOTER ********** -->
            <div id="dokuwiki__footer">
                <?php if($INFO['exists']): ?>
                <?php if (!empty($_SERVER['REMOTE_USER'])) { ?><div class="doc"><?php white_pageinfo() ?></div><?php } ?>
                <?php endif ?>
                <?php tpl_includeFile('sidebarfooter.html') ?>
                <?php
                 if ($translation) echo $translation->showTranslations();
                 ?>

                <?php tpl_license('badge', false, false) ?>
                <div class="footer">
                <?php
            		$footer_page = tpl_getConf('footer');
            		$footer = $lang2 .':'. $footer_page;
            		tpl_include_page($footer, 1, 1) ?>
                </div>



            </div><!-- /footer -->

            <?php tpl_includeFile('footer.html') ?>
        <?php html_msgarea() ?>

        </div><!-- /wrapper -->

    </div><!-- /site -->

    <div class="no"><?php tpl_indexerWebBug() /* provide DokuWiki housekeeping, required in all templates */ ?></div>
</body>
</html>
